const HDWalletProvider = require('@truffle/hdwallet-provider');
const path = require("path");

require('dotenv').config()

//let rinkebyProvider = new HDWalletProvider({
//  mnemonic: process.env.RINKEBY_MNEMONIC,
//  providerOrUrl: process.env.RINKEBY_RPC_URL
//});

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // to customize your Truffle configuration!
  networks: {
    develop: {
      port: 8545
    },
    ganache: {
      host: '127.0.0.1',
      port: 8545,
      network_id: '*',
    },
    rinkeby: {
        provider: null,//rinkebyProvider,
        network_id: 4,
        networkCheckTimeout: 1000000000,
        timeoutBlocks: 9000,
        skipDryRun: true
    },
    mainnet: {
        provider: null,
        network_id: 1,
        networkCheckTimeout: 1000000000,
      timeoutBlocks: 9000 //,
        // gas: gas,
        // gasPrice: gasPrice 
    }
  },
  solc: {
    optimizer: {
      enabled: true,
      runs: 200
    }
  },
  compilers: {
    solc: {
      version: '0.8.0',
    },
  },
    plugins: [
        'truffle-contract-size'
    ]
};
